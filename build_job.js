
const { writeFileSync,readFileSync } = require('fs');
const langs = ['en', 'it', 'de', 'da', 'se', 'fr'];
const { config } = JSON.parse(readFileSync('config.json', 'utf8'));

let pipeline = `
stages:
  - deploy
  - restart
`;

for (let lang of config) {
	pipeline += `
${lang.name}-sanity-deploy:
  stage: deploy
  script:
    - echo "deploy ${lang.restart}"
  when: 'manual'
${lang.name}-sanity-restart:
  stage: restart
  script:
    - echo "restart ${lang.restart}"
    - echo $CI_REGISTRY_IMAGE
  when: 'manual'
`;
}
writeFileSync('./langs-gitlab-ci.yml', pipeline, { encoding: 'utf-8' });
writeFileSync('./lan-gitlab-ci.yml', pipeline, { encoding: 'utf-8' });
