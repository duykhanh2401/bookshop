module.exports = {
	apps: [
		{
			script: 'server.js',
			instances: 1,
			autorestart: true,
			exec_mode: 'cluster',
			env: {
				NODE_ENV: 'production',
				DATABASE_PASSWORD: 'uEQZPuOpbCk4kCSnAivA',
				JWT_EXPIRES_IN: '30d',
				MAIL_PORT: '587',
				JWT_COOKIE_EXPIRES_IN: '30',
				ADMIN_EMAIL: 'bookshop2401@gmail.com',
				DATABASE:
					'mongodb://mongo:uEQZPuOpbCk4kCSnAivA@containers-us-west-36.railway.app:7708/Shop?authSource=admin',
				JWT_SECRET:
					'MAxWuHtzOhQEsVIUyAZWtz74Ov2LwFQdfv4ADmLA9aMqAoCr7qPJbwmUtQ8p',
				MAIL_HOST: 'smtp.gmail.com',
			},
			name: 'store',
		},
		// {
		// 	script: './service-worker/',
		// 	watch: ['./service-worker'],
		// },
	],

	deploy: {
		production: {
			user: 'root',
			ref: 'origin/main',
			repo: 'git@gitlab.com:duykhanh2401/bookshop.git',
			path: '/home/production',
			host: '128.199.201.207',
			// key: '/home/duykhanh/.ssh/id_rsa',
			'post-deploy':
				`git checkout main && git pull && git checkout tags/${process.env.TAG} &&npm install && npm install pm2 && sudo pm2 start ecosystem.config.js`,
			'pre-setup': `cd /home/production/source  && git pull && git checkout tags/${process.env.TAG}`,
			// 'pre-setup':
			// 	'sudo apt-get upgrade &&  sudo apt-get install python-software-properties && curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash – && nvm install 16.15.0  && npm install --global yarn@1.22.19	',
			ssh_options: ['StrictHostKeyChecking=no', 'PasswordAuthentication=no'],
		},
	},
};
