FROM node:16.15.0

WORKDIR /app

COPY . .

CMD ["sh","-c","tail -f /dev/null"]
